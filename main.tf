resource "aws_iam_role" "role_wt" {
  name = "iam_for_lambda_teste"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "lambda_wt" {
  filename         = "files/lambda_function_payload.zip"
  function_name    = "lambda_test_deploy"
  role             = "${aws_iam_role.role_wt.arn}"
  handler          = "exports.test"
  runtime          = "nodejs8.10"

  environment {
    variables = {
      foo = "bar"
    }
  }
}